import { Component, OnInit } from '@angular/core';
import { MenuController, NavController, Platform, LoadingController } from '@ionic/angular';

import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { DeviceMotion, DeviceMotionAccelerationData } from '@ionic-native/device-motion/ngx';
declare var google;
@Component({
  selector: 'app-settings',
  templateUrl: './settings.page.html',
  styleUrls: ['./settings.page.scss'],
})
export class SettingsPage{
  public openMenu: Boolean = false;
 
  public showLiveUpdatePopup: Boolean = false;
  constructor(private menu: MenuController, public navCtrl: NavController, public platform: Platform, public router: Router, public loadingController: LoadingController, public storage: Storage, public formBuilder: FormBuilder, private datePipe: DatePipe, public geolocation: Geolocation, public deviceMotion: DeviceMotion) {
    this.menu.enable(true);
  
  }



  searchFilter() {
    this.togglePopupMenu();
  }
  togglePopupMenu() {
   if (!this.showLiveUpdatePopup) {
    return this.openMenu = !this.openMenu;
   }
   
  }




  navigateTo(page) {
    this.router.navigate(['/' + page]);
  }

}
