import { Component, OnInit } from '@angular/core';
import { MenuController, LoadingController, AlertController } from '@ionic/angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { FCM } from '@ionic-native/fcm/ngx';
declare var device: any;
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  // public appVersion = '1.1.4';
  // public eyeOn = false;
  // public type = 'password';
  // public loginForm: FormGroup;
  constructor(public menu: MenuController, private formBuilder: FormBuilder, public loadingController: LoadingController,public router: Router, public storage: Storage, public fcm: FCM, public alertController: AlertController) {

  }
  navigateTo(page) {
    this.router.navigate(['/' + page]);
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.menu.enable(false);
  }


}
