(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./src/app/login/login.module.ts":
/*!***************************************!*\
  !*** ./src/app/login/login.module.ts ***!
  \***************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/login/login.page.ts");







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/login/login.page.html":
/*!***************************************!*\
  !*** ./src/app/login/login.page.html ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<ion-content>\n  <div id=\"loginPage\">\n  <div class=\"padded\">\n\n    <div class=\"login-form\">\n\n        <div class=\"logo-box\">\n            <img src=\"/assets/icon/ionic_base.png\" style=\"height: 72px;\">\n        </div>\n\n        <form   [formGroup]=\"loginForm\" id=\"myform\" style=\"width: 80%;margin:0 auto;\">\n\n            <div id=\"triangle\"></div>\n\n            <div class=\"login-box\">\n                <input type=\"text\" class=\"text-input text-input--material\" placeholder=\"Username\" id=\"username\"  formControlName=\"userName\" >\n\n                <div style=\"position: relative;\">\n                    <input [type]=\"type\"  class=\"text-input text-input--material\" id=\"loginpwd\" formControlName=\"password\" placeholder=\"Password\" required>\n                    <button class=\"pwd-align\" type=\"button\" >\n                        <i class=\"fa fa-eye\" *ngIf=\"eyeOn\" (click)=\"eyeOn = false; type = 'password'\"></i>\n                        <i class=\"fa fa-eye-slash\" *ngIf=\"!eyeOn\" (click)=\"eyeOn = true; type = 'text'\"></i>\n                    </button>\n                </div>\n\n                <br>\n                <ion-button class=\"login-button\" [disabled]=\"loginForm.invalid\" (click)=\"login()\">\n                    <span id=\"loginTxt\" class=\"login-fdbk\" style=\"font-family: Play;\">Log In</span>\n                    <span id=\"loginFdbk\" class=\"login-fdbk\" style=\"display: none;\">\n                   <i class=\"fa fa-circle-o-notch fa-spin\"></i>\n                   <span style=\"font-family: Play;\">Loading...</span>\n                  </span>\n                </ion-button>\n                <br>\n            </div>\n            <!-- <div><span style=\"font-size: 12px;color: #666666;font-family: Play;\">V <span class=\"app-version\">1.0.0</span></span></div> -->\n\n            <!-- <div><span style=\"font-size: 12px;color: #666666;font-family: Play;\">V <span class=\"app-version\">{{ appVersion }}</span></span></div> -->\n\n            <p class=\"p-by\">\n                <a href=\"javascript:void(0)\" style=\"color: #666666;font-size: 12px;text-decoration: none;\">\n                    Powered by <img src=\"/assets/icon/boodskap-logo.png\" style=\"height: 25px;vertical-align: -8px;\">  Boodskap\n                </a>\n            </p>\n          \n        </form>\n\n    </div>\n\n</div>\n</div>\n</ion-content>   \n"

/***/ }),

/***/ "./src/app/login/login.page.scss":
/*!***************************************!*\
  !*** ./src/app/login/login.page.scss ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".p-by {\n  font-family: Play;\n  background: rgba(255, 255, 255, 0.18);\n  margin-top: 20%;\n  display: inline-block;\n  padding: 0 10px;\n  border-radius: 5px;\n  font-size: 10px; }\n\n#loginPage .page__background {\n  background: #ffffff;\n  /*opacity: 0.7;*/\n  /*background-image: url('img/amb.jpg') !important;*/\n  /*-webkit-background-size: cover;*/\n  /*-moz-background-size: center cover;*/\n  /*background-size: cover;*/\n  /*background-position: center top;*/ }\n\n#loginPage .login-form {\n  text-align: center;\n  margin: 60px auto 0;\n  margin-top: 10%;\n  width: 100%;\n  background: none;\n  padding: 25px 0; }\n\n#loginPage #username, #loginpwd {\n  display: block;\n  width: 100%;\n  margin: 0 auto;\n  outline: none;\n  height: 100%;\n  padding: 15px 20px;\n  color: #333333;\n  font-size: 16px;\n  background: #eeeeee;\n  margin-top: 7px;\n  border: 0;\n  border-bottom: 1px solid #cccccc;\n  border-radius: 0; }\n\n#loginPage #username::-webkit-input-placeholder, #loginpwd::-webkit-input-placeholder {\n  color: #999999; }\n\n#loginPage #username:-ms-input-placeholder, #loginpwd:-ms-input-placeholder {\n  color: #999999; }\n\n#loginPage #username::-ms-input-placeholder, #loginpwd::-ms-input-placeholder {\n  color: #999999; }\n\n#loginPage #username::placeholder, #loginpwd::placeholder {\n  color: #999999; }\n\n#loginPage .login-button {\n  width: 100%;\n  margin: 0 auto;\n  border-radius: 5px;\n  background: #112e50;\n  color: #ffffff;\n  font-weight: 600; }\n\n#loginPage .big-title {\n  font-size: 12px;\n  margin: 0;\n  width: 130px;\n  display: inline-block;\n  padding: 5px 15px;\n  background: rgba(255, 255, 255, 0.18);\n  border-radius: 10px;\n  margin-bottom: 20px; }\n\n#loginPage .forgot-password {\n  display: block;\n  margin: 8px auto 0 auto;\n  font-size: 14px;\n  color: #112e50; }\n\n#loginPage .pwd-align {\n  position: absolute;\n  right: 6px;\n  bottom: 15px;\n  background: transparent;\n  border: 0;\n  font-size: 20px;\n  outline: none; }\n\n#loginPage .slide {\n  width: 90%;\n  margin: 0 auto; }\n\n#loginPage .pwd-align i {\n  color: #666666; }\n\n#triangle {\n  width: 0;\n  height: 0;\n  border-bottom: 23px solid #fff;\n  border-left: 25px solid transparent;\n  border-right: 25px solid transparent;\n  /* text-align: center; */\n  margin: 0 auto; }\n\n.logo-box {\n  width: 65%;\n  display: inline-block;\n  background: #ffffff;\n  padding: 7px;\n  border-radius: 8px;\n  margin-top: 1em;\n  margin-bottom: 1em; }\n\n.app-name {\n  font-family: 'Play';\n  color: #666666;\n  font-size: 20px; }\n\n.login-box {\n  background: #ffffff;\n  border: 1px solid #ffffff;\n  border-radius: 3px;\n  padding: 10px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2JhdmFuaS9EZXNrdG9wL2Jhc2UtZnJhbWV3b3JrLWlvbmljL3NyYy9hcHAvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUJBQWlCO0VBQ2pCLHFDQUFrQztFQUNsQyxlQUFlO0VBQ2YscUJBQXFCO0VBQ3JCLGVBQWU7RUFDZixrQkFBa0I7RUFDbEIsZUFBZSxFQUFBOztBQUdqQjtFQUNFLG1CQUFtQjtFQUNuQixnQkFBQTtFQUNBLG1EQUFBO0VBQ0Esa0NBQUE7RUFDQSxzQ0FBQTtFQUNBLDBCQUFBO0VBQ0EsbUNBQUEsRUFBb0M7O0FBR3RDO0VBQ0Usa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixlQUFjO0VBQ2QsV0FBVztFQUNYLGdCQUFnQjtFQUNoQixlQUFlLEVBQUE7O0FBR2pCO0VBQ0UsY0FBYztFQUNkLFdBQVc7RUFDWCxjQUFjO0VBQ2QsYUFBYTtFQUNiLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsY0FBYztFQUNkLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsZUFBZTtFQUNmLFNBQVM7RUFDVCxnQ0FBZ0M7RUFDaEMsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBQ0UsY0FBYyxFQUFBOztBQURoQjtFQUNFLGNBQWMsRUFBQTs7QUFEaEI7RUFDRSxjQUFjLEVBQUE7O0FBRGhCO0VBQ0UsY0FBYyxFQUFBOztBQUdoQjtFQUNFLFdBQVc7RUFDWCxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsZ0JBQWdCLEVBQUE7O0FBRWxCO0VBQ0UsZUFBZTtFQUNmLFNBQVM7RUFDVCxZQUFZO0VBQ1oscUJBQXFCO0VBQ3JCLGlCQUFpQjtFQUNqQixxQ0FBa0M7RUFDbEMsbUJBQW1CO0VBQ25CLG1CQUFtQixFQUFBOztBQUVyQjtFQUNFLGNBQWM7RUFDZCx1QkFBdUI7RUFDdkIsZUFBZTtFQUNmLGNBQWMsRUFBQTs7QUFFaEI7RUFDRSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLFlBQVk7RUFDWix1QkFBdUI7RUFDdkIsU0FBUztFQUNULGVBQWU7RUFDZixhQUFhLEVBQUE7O0FBRWY7RUFDRSxVQUFVO0VBQ1YsY0FBYyxFQUFBOztBQUVoQjtFQUNFLGNBQWMsRUFBQTs7QUFFaEI7RUFDRSxRQUFRO0VBQ1IsU0FBUztFQUNULDhCQUE4QjtFQUM5QixtQ0FBbUM7RUFDbkMsb0NBQW9DO0VBQ3BDLHdCQUFBO0VBQ0EsY0FBYyxFQUFBOztBQUVoQjtFQUNFLFVBQVU7RUFDVixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLGtCQUFrQixFQUFBOztBQUVwQjtFQUNFLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsZUFBZSxFQUFBOztBQUVqQjtFQUNFLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLGFBQWEsRUFBQSIsImZpbGUiOiJzcmMvYXBwL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5wLWJ5e1xuICBmb250LWZhbWlseTogUGxheTtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSwwLjE4KTtcbiAgbWFyZ2luLXRvcDogMjAlO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmc6IDAgMTBweDtcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBmb250LXNpemU6IDEwcHg7XG59XG5cbiNsb2dpblBhZ2UgLnBhZ2VfX2JhY2tncm91bmQge1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICAvKm9wYWNpdHk6IDAuNzsqL1xuICAvKmJhY2tncm91bmQtaW1hZ2U6IHVybCgnaW1nL2FtYi5qcGcnKSAhaW1wb3J0YW50OyovXG4gIC8qLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyOyovXG4gIC8qLW1vei1iYWNrZ3JvdW5kLXNpemU6IGNlbnRlciBjb3ZlcjsqL1xuICAvKmJhY2tncm91bmQtc2l6ZTogY292ZXI7Ki9cbiAgLypiYWNrZ3JvdW5kLXBvc2l0aW9uOiBjZW50ZXIgdG9wOyovXG59XG5cbiNsb2dpblBhZ2UgLmxvZ2luLWZvcm0ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbjogNjBweCBhdXRvIDA7XG4gIG1hcmdpbi10b3A6MTAlO1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgcGFkZGluZzogMjVweCAwO1xufVxuXG4jbG9naW5QYWdlICN1c2VybmFtZSwgI2xvZ2lucHdkIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IDAgYXV0bztcbiAgb3V0bGluZTogbm9uZTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBwYWRkaW5nOiAxNXB4IDIwcHg7XG4gIGNvbG9yOiAjMzMzMzMzO1xuICBmb250LXNpemU6IDE2cHg7XG4gIGJhY2tncm91bmQ6ICNlZWVlZWU7XG4gIG1hcmdpbi10b3A6IDdweDtcbiAgYm9yZGVyOiAwO1xuICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2NjY2NjYztcbiAgYm9yZGVyLXJhZGl1czogMDtcbn1cblxuI2xvZ2luUGFnZSAjdXNlcm5hbWU6OnBsYWNlaG9sZGVyLCAjbG9naW5wd2Q6OnBsYWNlaG9sZGVyIHtcbiAgY29sb3I6ICM5OTk5OTk7XG59XG5cbiNsb2dpblBhZ2UgLmxvZ2luLWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBtYXJnaW46IDAgYXV0bztcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xuICBiYWNrZ3JvdW5kOiAjMzk3ZmZkO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cbiNsb2dpblBhZ2UgLmJpZy10aXRsZXtcbiAgZm9udC1zaXplOiAxMnB4O1xuICBtYXJnaW46IDA7XG4gIHdpZHRoOiAxMzBweDtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICBwYWRkaW5nOiA1cHggMTVweDtcbiAgYmFja2dyb3VuZDogcmdiYSgyNTUsMjU1LDI1NSwwLjE4KTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cbiNsb2dpblBhZ2UgLmZvcmdvdC1wYXNzd29yZCB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDhweCBhdXRvIDAgYXV0bztcbiAgZm9udC1zaXplOiAxNHB4O1xuICBjb2xvcjogIzM5N2ZmZDtcbn1cbiNsb2dpblBhZ2UgLnB3ZC1hbGlnbiB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDZweDtcbiAgYm90dG9tOiAxNXB4O1xuICBiYWNrZ3JvdW5kOiB0cmFuc3BhcmVudDtcbiAgYm9yZGVyOiAwO1xuICBmb250LXNpemU6IDIwcHg7XG4gIG91dGxpbmU6IG5vbmU7XG59XG4jbG9naW5QYWdlIC5zbGlkZXtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG4jbG9naW5QYWdlIC5wd2QtYWxpZ24gaXtcbiAgY29sb3I6ICM2NjY2NjY7XG59XG4jdHJpYW5nbGUge1xuICB3aWR0aDogMDtcbiAgaGVpZ2h0OiAwO1xuICBib3JkZXItYm90dG9tOiAyM3B4IHNvbGlkICNmZmY7XG4gIGJvcmRlci1sZWZ0OiAyNXB4IHNvbGlkIHRyYW5zcGFyZW50O1xuICBib3JkZXItcmlnaHQ6IDI1cHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIC8qIHRleHQtYWxpZ246IGNlbnRlcjsgKi9cbiAgbWFyZ2luOiAwIGF1dG87XG59XG4ubG9nby1ib3h7XG4gIHdpZHRoOiA2NSU7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgcGFkZGluZzogN3B4O1xuICBib3JkZXItcmFkaXVzOiA4cHg7XG4gIG1hcmdpbi10b3A6IDFlbTtcbiAgbWFyZ2luLWJvdHRvbTogMWVtO1xufVxuLmFwcC1uYW1le1xuICBmb250LWZhbWlseTogJ1BsYXknO1xuICBjb2xvcjogIzY2NjY2NjtcbiAgZm9udC1zaXplOiAyMHB4O1xufVxuLmxvZ2luLWJveHtcbiAgYmFja2dyb3VuZDogI2ZmZmZmZjtcbiAgYm9yZGVyOiAxcHggc29saWQgI2ZmZmZmZjtcbiAgYm9yZGVyLXJhZGl1czogM3B4O1xuICBwYWRkaW5nOiAxMHB4O1xufVxuIl19 */"

/***/ }),

/***/ "./src/app/login/login.page.ts":
/*!*************************************!*\
  !*** ./src/app/login/login.page.ts ***!
  \*************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/fcm/ngx */ "./node_modules/@ionic-native/fcm/ngx/index.js");









var LoginPage = /** @class */ (function () {
    function LoginPage(menu, formBuilder, loadingController, commonService, router, storage, fcm, alertController) {
        var _this = this;
        this.menu = menu;
        this.formBuilder = formBuilder;
        this.loadingController = loadingController;
        this.commonService = commonService;
        this.router = router;
        this.storage = storage;
        this.fcm = fcm;
        this.alertController = alertController;
        this.appVersion = '1.1.4';
        this.eyeOn = false;
        this.type = 'password';
        this.loginForm = this.formBuilder.group({
            userName: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required],
        });
        this.storage.get('userDetails').then(function (val) {
            console.log('userDetails =', val);
            if (val !== undefined && val !== null) {
                _this.router.navigate(['/dashboard']);
            }
        });
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.ionViewWillEnter = function () {
        this.menu.enable(false);
    };
    LoginPage.prototype.login = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var loading, actionURL;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.loadingController.create({
                            message: 'Loading...'
                        })];
                    case 1:
                        loading = _a.sent();
                        return [4 /*yield*/, loading.present()];
                    case 2:
                        _a.sent();
                        actionURL = "domain/login/" + this.loginForm.value.userName + "/" + this.loginForm.value.password + '?targetDomainKey=' + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].DOMAIN_KEY;
                        return [4 /*yield*/, this.commonService.getAllCall(actionURL)
                                .subscribe(function (res) {
                                console.log(res);
                                if (res) {
                                    var actionUrl = 'sql/exec/multi/' + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_TOKEN;
                                    var data = {
                                        "queries": [
                                            { "type": "SELECT", "query": "SELECT * FROM " + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].DRIVER_MASTER + " WHERE email='" + _this.loginForm.value.userName + "'" },
                                        ]
                                    };
                                    _this.commonService.postCall(actionUrl, JSON.stringify(data))
                                        .subscribe(function (result) {
                                        res['driverObj'] = result.results[0].records[0];
                                        res['driverObj']['driverimg'] = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].fileUrl + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_TOKEN + '/' + res['driverObj']['driverimg'];
                                        _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_KEY = res.apiKey;
                                        _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_TOKEN = res.token;
                                        _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].DOMAIN_KEY = res.domainKey;
                                        _this.storage.set('userDetails', res);
                                        /////////////////// FCM //////////////////////
                                        _this.fcm.subscribeToTopic('drivermodule');
                                        _this.fcm.getToken().then(function (token) {
                                            var path = "fcm/register/" + _environments_environment_prod__WEBPACK_IMPORTED_MODULE_5__["environment"].API_TOKEN + "/" + res['driverObj'].email + "/" + device.model + "/" + device.version + "/" + token;
                                            _this.commonService.getAllCall(path)
                                                .subscribe(function (res) {
                                                _this.commonService.presentToast('Device ID registered successfully');
                                            });
                                        });
                                        _this.fcm.onNotification().subscribe(function (data) {
                                            _this.presentAlertConfirm('You got new trip!');
                                        });
                                        /////////////////// FCM //////////////////////
                                        _this.router.navigate(['/dashboard']);
                                    });
                                }
                                else {
                                    _this.commonService.presentToast('Username/Password Invalid');
                                }
                                loading.dismiss();
                            }, function (err) {
                                console.log(err);
                                loading.dismiss();
                            })];
                    case 3:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage.prototype.presentAlertConfirm = function (status) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'New Trip Alert',
                            message: status,
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function (blah) {
                                        _this.commonService.presentToast('Your option have been saved.');
                                    }
                                }, {
                                    text: 'Okay',
                                    handler: function () {
                                        _this.router.navigate(['/home']);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _services_common_service__WEBPACK_IMPORTED_MODULE_4__["CommonService"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"], _ionic_storage__WEBPACK_IMPORTED_MODULE_7__["Storage"], _ionic_native_fcm_ngx__WEBPACK_IMPORTED_MODULE_8__["FCM"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["AlertController"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module.js.map