(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["list-list-module"],{

/***/ "./src/app/list/list.module.ts":
/*!*************************************!*\
  !*** ./src/app/list/list.module.ts ***!
  \*************************************/
/*! exports provided: ListPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPageModule", function() { return ListPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _list_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./list.page */ "./src/app/list/list.page.ts");







var ListPageModule = /** @class */ (function () {
    function ListPageModule() {
    }
    ListPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _list_page__WEBPACK_IMPORTED_MODULE_6__["ListPage"]
                    }
                ])
            ],
            declarations: [_list_page__WEBPACK_IMPORTED_MODULE_6__["ListPage"]]
        })
    ], ListPageModule);
    return ListPageModule;
}());



/***/ }),

/***/ "./src/app/list/list.page.html":
/*!*************************************!*\
  !*** ./src/app/list/list.page.html ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"headers\">\n\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button></ion-menu-button>\n    </ion-buttons>\n    <ion-title slot=\"start\">\n      Templates\n\n    </ion-title>\n\n \n  </ion-toolbar>\n</ion-header>\n\n<ion-content>\n  \n</ion-content>"

/***/ }),

/***/ "./src/app/list/list.page.scss":
/*!*************************************!*\
  !*** ./src/app/list/list.page.scss ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* Blue Flat Button\n==================================================*/\n.btn-xlarge {\n  border-radius: 16px;\n  position: relative;\n  vertical-align: center;\n  color: white;\n  text-align: center;\n  text-shadow: 0 1px 2px rgba(0, 0, 0, 0.25);\n  background: #666666;\n  border: 0;\n  border-bottom: 3px solid #4db26d;\n  cursor: pointer;\n  box-shadow: inset 0 -3px #4db26d; }\n.btn-xlarge:active {\n  top: 2px;\n  outline: none;\n  box-shadow: none; }\n.btn-xlarge:hover {\n  background: #45E1E8; }\n.toolbar-background {\n  background: #666666 !important; }\n.list-group {\n  border-radius: 0; }\n.list-group .list-group-item {\n  background-color: transparent;\n  overflow: hidden;\n  border: 0;\n  border-radius: 0;\n  padding: 0 16px; }\n.list-group .list-group-item .row-picture,\n.list-group .list-group-item .row-action-primary {\n  float: left;\n  display: inline-block;\n  padding-right: 16px;\n  padding-top: 8px; }\n.list-group .list-group-item .row-picture img,\n.list-group .list-group-item .row-action-primary img,\n.list-group .list-group-item .row-picture i,\n.list-group .list-group-item .row-action-primary i,\n.list-group .list-group-item .row-picture label,\n.list-group .list-group-item .row-action-primary label {\n  display: block;\n  width: 56px;\n  height: 56px; }\n.list-group .list-group-item .row-picture img,\n.list-group .list-group-item .row-action-primary img {\n  background: rgba(0, 0, 0, 0.1);\n  padding: 1px; }\n.list-group .list-group-item .row-picture img.circle,\n.list-group .list-group-item .row-action-primary img.circle {\n  border-radius: 100%; }\n.list-group .list-group-item .row-picture i,\n.list-group .list-group-item .row-action-primary i {\n  background: rgba(0, 0, 0, 0.25);\n  border-radius: 100%;\n  text-align: center;\n  line-height: 56px;\n  font-size: 20px;\n  color: white; }\n.list-group .list-group-item .row-picture label,\n.list-group .list-group-item .row-action-primary label {\n  margin-left: 7px;\n  margin-right: -7px;\n  margin-top: 5px;\n  margin-bottom: -5px; }\n.list-group .list-group-item .row-content {\n  display: inline-block;\n  width: calc(100% - 92px);\n  min-height: 66px; }\n.list-group .list-group-item .row-content .action-secondary {\n  position: absolute;\n  right: 16px;\n  top: 16px; }\n.list-group .list-group-item .row-content .action-secondary i {\n  font-size: 20px;\n  color: rgba(0, 0, 0, 0.25);\n  cursor: pointer; }\n.list-group .list-group-item .row-content .action-secondary ~ * {\n  max-width: calc(100% - 30px); }\n.list-group .list-group-item .row-content .least-content {\n  position: absolute;\n  right: 16px;\n  top: 0px;\n  color: rgba(0, 0, 0, 0.54);\n  font-size: 14px; }\n.list-group .list-group-item .list-group-item-heading {\n  color: rgba(0, 0, 0, 0.77);\n  font-size: 20px;\n  line-height: 29px; }\n.list-group .list-group-separator {\n  clear: both;\n  overflow: hidden;\n  margin-top: 10px;\n  margin-bottom: 10px; }\n.list-group .list-group-separator:before {\n  content: \"\";\n  width: calc(100% - 90px);\n  border-bottom: 1px solid rgba(0, 0, 0, 0.1);\n  float: right; }\n.bg-profile {\n  background-color: #3498DB !important;\n  height: 150px;\n  z-index: 1; }\n.bg-bottom {\n  height: 100px;\n  margin-left: 30px; }\n.img-profile {\n  display: inline-block !important;\n  background-color: #fff;\n  border-radius: 6px;\n  margin-top: -50%;\n  padding: 1px;\n  vertical-align: bottom;\n  border: 2px solid #fff;\n  box-sizing: border-box;\n  color: #fff;\n  z-index: 2; }\n.row-float {\n  margin-top: -40px; }\n.explore a {\n  color: green;\n  font-size: 13px;\n  font-weight: 600; }\n.twitter a {\n  color: #4099FF; }\n.img-box {\n  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);\n  border-radius: 2px;\n  border: 0; }\nion-tab-button.tab-selected.tab-has-label.tab-has-icon.tab-layout-icon-top.ion-activatable.hydrated {\n  color: #8c8c8c; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2JhdmFuaS9EZXNrdG9wL2Jhc2UtZnJhbWV3b3JrLWlvbmljL3NyYy9hcHAvbGlzdC9saXN0LnBhZ2Uuc2NzcyIsInNyYy9hcHAvbGlzdC9saXN0LnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDQTttRENBbUQ7QURFbkQ7RUFDSSxtQkFBbUI7RUFDbkIsa0JBQWtCO0VBQ2xCLHNCQUFzQjtFQUN0QixZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLDBDQUEwQztFQUMxQyxtQkFBbUI7RUFDbkIsU0FBUztFQUNULGdDQUFnQztFQUNoQyxlQUFlO0VBQ2YsZ0NBQWdDLEVBQUE7QUFFbEM7RUFDRSxRQUFRO0VBQ1IsYUFBYTtFQUViLGdCQUFnQixFQUFBO0FBRWxCO0VBQ0UsbUJBQW1CLEVBQUE7QUFFckI7RUFDRSw4QkFBOEIsRUFBQTtBQUVsQztFQUFhLGdCQUFnQixFQUFBO0FBQzdCO0VBQThCLDZCQUE2QjtFQUFDLGdCQUFnQjtFQUFDLFNBQVM7RUFBQyxnQkFBZ0I7RUFBQyxlQUFlLEVBQUE7QUFDdkg7O0VBQ2tELFdBQVc7RUFBQyxxQkFBcUI7RUFBQyxtQkFBbUI7RUFBQyxnQkFBZ0IsRUFBQTtBQUN4SDs7Ozs7O0VBS3dELGNBQWM7RUFBQyxXQUFXO0VBQUMsWUFBWSxFQUFBO0FBQy9GOztFQUNzRCw4QkFBOEI7RUFBQyxZQUFZLEVBQUE7QUFDakc7O0VBQzZELG1CQUFtQixFQUFBO0FBQ2hGOztFQUNvRCwrQkFBK0I7RUFBQyxtQkFBbUI7RUFBQyxrQkFBa0I7RUFBQyxpQkFBaUI7RUFBQyxlQUFlO0VBQUMsWUFBWSxFQUFBO0FBQ3pLOztFQUN3RCxnQkFBZ0I7RUFBQyxrQkFBa0I7RUFBQyxlQUFlO0VBQUMsbUJBQW1CLEVBQUE7QUFDL0g7RUFBMkMscUJBQXFCO0VBQUMsd0JBQXdCO0VBQUMsZ0JBQWdCLEVBQUE7QUFDMUc7RUFBNkQsa0JBQWtCO0VBQUMsV0FBVztFQUFDLFNBQVMsRUFBQTtBQUNyRztFQUErRCxlQUFlO0VBQUMsMEJBQTBCO0VBQUMsZUFBZSxFQUFBO0FBQ3pIO0VBQWlFLDRCQUE0QixFQUFBO0FBQzdGO0VBQTBELGtCQUFrQjtFQUFDLFdBQVc7RUFBQyxRQUFRO0VBQUMsMEJBQTBCO0VBQUMsZUFBZSxFQUFBO0FBQzVJO0VBQXVELDBCQUEwQjtFQUFDLGVBQWU7RUFBQyxpQkFBaUIsRUFBQTtBQUNuSDtFQUFtQyxXQUFXO0VBQUMsZ0JBQWdCO0VBQUMsZ0JBQWdCO0VBQUMsbUJBQW1CLEVBQUE7QUFDcEc7RUFBMEMsV0FBVztFQUFDLHdCQUF3QjtFQUFDLDJDQUEyQztFQUFDLFlBQVksRUFBQTtBQUV2STtFQUFZLG9DQUFvQztFQUFDLGFBQWE7RUFBQyxVQUFVLEVBQUE7QUFDekU7RUFBVyxhQUFhO0VBQUMsaUJBQWlCLEVBQUE7QUFDMUM7RUFBYSxnQ0FBZ0M7RUFBQyxzQkFBc0I7RUFBQyxrQkFBa0I7RUFBQyxnQkFBZ0I7RUFBQyxZQUFZO0VBQUMsc0JBQXNCO0VBQUMsc0JBQXNCO0VBQTZCLHNCQUFzQjtFQUFDLFdBQVc7RUFBQyxVQUFVLEVBQUE7QUFDN087RUFBVyxpQkFBaUIsRUFBQTtBQUM1QjtFQUFZLFlBQVk7RUFBRSxlQUFlO0VBQUMsZ0JBQWdCLEVBQUE7QUFDMUQ7RUFBWSxjQUFhLEVBQUE7QUFDekI7RUFBUyx3RUFBK0Q7RUFBQyxrQkFBa0I7RUFBQyxTQUFTLEVBQUE7QUFDckc7RUFDRSxjQUFjLEVBQUEiLCJmaWxlIjoic3JjL2FwcC9saXN0L2xpc3QucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXG4vKiBCbHVlIEZsYXQgQnV0dG9uXG49PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PSovXG4uYnRuLXhsYXJnZXtcbiAgICBib3JkZXItcmFkaXVzOiAxNnB4O1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB2ZXJ0aWNhbC1hbGlnbjogY2VudGVyO1xuICAgIGNvbG9yOiB3aGl0ZTtcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgdGV4dC1zaGFkb3c6IDAgMXB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICAgIGJhY2tncm91bmQ6ICM2NjY2NjY7XG4gICAgYm9yZGVyOiAwO1xuICAgIGJvcmRlci1ib3R0b206IDNweCBzb2xpZCAjNGRiMjZkO1xuICAgIGN1cnNvcjogcG9pbnRlcjtcbiAgICBib3gtc2hhZG93OiBpbnNldCAwIC0zcHggIzRkYjI2ZDtcbn1cbiAgLmJ0bi14bGFyZ2U6YWN0aXZlIHtcbiAgICB0b3A6IDJweDtcbiAgICBvdXRsaW5lOiBub25lO1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZTtcbiAgICBib3gtc2hhZG93OiBub25lO1xuICB9XG4gIC5idG4teGxhcmdlOmhvdmVyIHtcbiAgICBiYWNrZ3JvdW5kOiAjNDVFMUU4O1xuICB9XG4gIC50b29sYmFyLWJhY2tncm91bmQge1xuICAgIGJhY2tncm91bmQ6ICM2NjY2NjYgIWltcG9ydGFudDtcbn1cbi5saXN0LWdyb3VwIHtib3JkZXItcmFkaXVzOiAwO31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0ge2JhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O292ZXJmbG93OiBoaWRkZW47Ym9yZGVyOiAwO2JvcmRlci1yYWRpdXM6IDA7cGFkZGluZzogMCAxNnB4O31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IHtmbG9hdDogbGVmdDtkaXNwbGF5OiBpbmxpbmUtYmxvY2s7cGFkZGluZy1yaWdodDogMTZweDtwYWRkaW5nLXRvcDogOHB4O31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlIGltZyxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSBpbWcsXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctcGljdHVyZSBpLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGksXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctcGljdHVyZSBsYWJlbCxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSBsYWJlbCB7ZGlzcGxheTogYmxvY2s7d2lkdGg6IDU2cHg7aGVpZ2h0OiA1NnB4O31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlIGltZyxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSBpbWcge2JhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4xKTtwYWRkaW5nOiAxcHg7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgaW1nLmNpcmNsZSxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSBpbWcuY2lyY2xlIHtib3JkZXItcmFkaXVzOiAxMDAlO31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlIGksXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkgaSB7YmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjI1KTtib3JkZXItcmFkaXVzOiAxMDAlO3RleHQtYWxpZ246IGNlbnRlcjtsaW5lLWhlaWdodDogNTZweDtmb250LXNpemU6IDIwcHg7Y29sb3I6IHdoaXRlO31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlIGxhYmVsLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGxhYmVsIHttYXJnaW4tbGVmdDogN3B4O21hcmdpbi1yaWdodDogLTdweDttYXJnaW4tdG9wOiA1cHg7bWFyZ2luLWJvdHRvbTogLTVweDt9XG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctY29udGVudCB7ZGlzcGxheTogaW5saW5lLWJsb2NrO3dpZHRoOiBjYWxjKDEwMCUgLSA5MnB4KTttaW4taGVpZ2h0OiA2NnB4O31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1jb250ZW50IC5hY3Rpb24tc2Vjb25kYXJ5IHtwb3NpdGlvbjogYWJzb2x1dGU7cmlnaHQ6IDE2cHg7dG9wOiAxNnB4O31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1jb250ZW50IC5hY3Rpb24tc2Vjb25kYXJ5IGkge2ZvbnQtc2l6ZTogMjBweDtjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjI1KTtjdXJzb3I6IHBvaW50ZXI7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWNvbnRlbnQgLmFjdGlvbi1zZWNvbmRhcnkgfiAqIHttYXgtd2lkdGg6IGNhbGMoMTAwJSAtIDMwcHgpO31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1jb250ZW50IC5sZWFzdC1jb250ZW50IHtwb3NpdGlvbjogYWJzb2x1dGU7cmlnaHQ6IDE2cHg7dG9wOiAwcHg7Y29sb3I6IHJnYmEoMCwgMCwgMCwgMC41NCk7Zm9udC1zaXplOiAxNHB4O31cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLmxpc3QtZ3JvdXAtaXRlbS1oZWFkaW5nIHtjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjc3KTtmb250LXNpemU6IDIwcHg7bGluZS1oZWlnaHQ6IDI5cHg7fVxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtc2VwYXJhdG9yIHtjbGVhcjogYm90aDtvdmVyZmxvdzogaGlkZGVuO21hcmdpbi10b3A6IDEwcHg7bWFyZ2luLWJvdHRvbTogMTBweDt9XG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1zZXBhcmF0b3I6YmVmb3JlIHtjb250ZW50OiBcIlwiO3dpZHRoOiBjYWxjKDEwMCUgLSA5MHB4KTtib3JkZXItYm90dG9tOiAxcHggc29saWQgcmdiYSgwLCAwLCAwLCAwLjEpO2Zsb2F0OiByaWdodDt9XG5cbi5iZy1wcm9maWxle2JhY2tncm91bmQtY29sb3I6ICMzNDk4REIgIWltcG9ydGFudDtoZWlnaHQ6IDE1MHB4O3otaW5kZXg6IDE7fVxuLmJnLWJvdHRvbXtoZWlnaHQ6IDEwMHB4O21hcmdpbi1sZWZ0OiAzMHB4O31cbi5pbWctcHJvZmlsZXtkaXNwbGF5OiBpbmxpbmUtYmxvY2sgIWltcG9ydGFudDtiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO2JvcmRlci1yYWRpdXM6IDZweDttYXJnaW4tdG9wOiAtNTAlO3BhZGRpbmc6IDFweDt2ZXJ0aWNhbC1hbGlnbjogYm90dG9tO2JvcmRlcjogMnB4IHNvbGlkICNmZmY7LW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O2JveC1zaXppbmc6IGJvcmRlci1ib3g7Y29sb3I6ICNmZmY7ei1pbmRleDogMjt9XG4ucm93LWZsb2F0e21hcmdpbi10b3A6IC00MHB4O31cbi5leHBsb3JlIGEge2NvbG9yOiBncmVlbjsgZm9udC1zaXplOiAxM3B4O2ZvbnQtd2VpZ2h0OiA2MDB9XG4udHdpdHRlciBhIHtjb2xvcjojNDA5OUZGfVxuLmltZy1ib3h7Ym94LXNoYWRvdzogMCAzcHggNnB4IHJnYmEoMCwwLDAsLjE2KSwwIDNweCA2cHggcmdiYSgwLDAsMCwuMjMpO2JvcmRlci1yYWRpdXM6IDJweDtib3JkZXI6IDA7fVxuaW9uLXRhYi1idXR0b24udGFiLXNlbGVjdGVkLnRhYi1oYXMtbGFiZWwudGFiLWhhcy1pY29uLnRhYi1sYXlvdXQtaWNvbi10b3AuaW9uLWFjdGl2YXRhYmxlLmh5ZHJhdGVkIHtcbiAgY29sb3I6ICM4YzhjOGM7XG59IiwiLyogQmx1ZSBGbGF0IEJ1dHRvblxuPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT0qL1xuLmJ0bi14bGFyZ2Uge1xuICBib3JkZXItcmFkaXVzOiAxNnB4O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHZlcnRpY2FsLWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB3aGl0ZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LXNoYWRvdzogMCAxcHggMnB4IHJnYmEoMCwgMCwgMCwgMC4yNSk7XG4gIGJhY2tncm91bmQ6ICM2NjY2NjY7XG4gIGJvcmRlcjogMDtcbiAgYm9yZGVyLWJvdHRvbTogM3B4IHNvbGlkICM0ZGIyNmQ7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgYm94LXNoYWRvdzogaW5zZXQgMCAtM3B4ICM0ZGIyNmQ7IH1cblxuLmJ0bi14bGFyZ2U6YWN0aXZlIHtcbiAgdG9wOiAycHg7XG4gIG91dGxpbmU6IG5vbmU7XG4gIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZTtcbiAgYm94LXNoYWRvdzogbm9uZTsgfVxuXG4uYnRuLXhsYXJnZTpob3ZlciB7XG4gIGJhY2tncm91bmQ6ICM0NUUxRTg7IH1cblxuLnRvb2xiYXItYmFja2dyb3VuZCB7XG4gIGJhY2tncm91bmQ6ICM2NjY2NjYgIWltcG9ydGFudDsgfVxuXG4ubGlzdC1ncm91cCB7XG4gIGJvcmRlci1yYWRpdXM6IDA7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBib3JkZXI6IDA7XG4gIGJvcmRlci1yYWRpdXM6IDA7XG4gIHBhZGRpbmc6IDAgMTZweDsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctcGljdHVyZSxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSB7XG4gIGZsb2F0OiBsZWZ0O1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gIHBhZGRpbmctcmlnaHQ6IDE2cHg7XG4gIHBhZGRpbmctdG9wOiA4cHg7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgaW1nLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGltZyxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlIGksXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkgaSxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlIGxhYmVsLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGxhYmVsIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiA1NnB4O1xuICBoZWlnaHQ6IDU2cHg7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgaW1nLFxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWFjdGlvbi1wcmltYXJ5IGltZyB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgcGFkZGluZzogMXB4OyB9XG5cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1waWN0dXJlIGltZy5jaXJjbGUsXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkgaW1nLmNpcmNsZSB7XG4gIGJvcmRlci1yYWRpdXM6IDEwMCU7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgaSxcbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1hY3Rpb24tcHJpbWFyeSBpIHtcbiAgYmFja2dyb3VuZDogcmdiYSgwLCAwLCAwLCAwLjI1KTtcbiAgYm9yZGVyLXJhZGl1czogMTAwJTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBsaW5lLWhlaWdodDogNTZweDtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBjb2xvcjogd2hpdGU7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LXBpY3R1cmUgbGFiZWwsXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctYWN0aW9uLXByaW1hcnkgbGFiZWwge1xuICBtYXJnaW4tbGVmdDogN3B4O1xuICBtYXJnaW4tcmlnaHQ6IC03cHg7XG4gIG1hcmdpbi10b3A6IDVweDtcbiAgbWFyZ2luLWJvdHRvbTogLTVweDsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctY29udGVudCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IGNhbGMoMTAwJSAtIDkycHgpO1xuICBtaW4taGVpZ2h0OiA2NnB4OyB9XG5cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1jb250ZW50IC5hY3Rpb24tc2Vjb25kYXJ5IHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICByaWdodDogMTZweDtcbiAgdG9wOiAxNnB4OyB9XG5cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLnJvdy1jb250ZW50IC5hY3Rpb24tc2Vjb25kYXJ5IGkge1xuICBmb250LXNpemU6IDIwcHg7XG4gIGNvbG9yOiByZ2JhKDAsIDAsIDAsIDAuMjUpO1xuICBjdXJzb3I6IHBvaW50ZXI7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtaXRlbSAucm93LWNvbnRlbnQgLmFjdGlvbi1zZWNvbmRhcnkgfiAqIHtcbiAgbWF4LXdpZHRoOiBjYWxjKDEwMCUgLSAzMHB4KTsgfVxuXG4ubGlzdC1ncm91cCAubGlzdC1ncm91cC1pdGVtIC5yb3ctY29udGVudCAubGVhc3QtY29udGVudCB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgcmlnaHQ6IDE2cHg7XG4gIHRvcDogMHB4O1xuICBjb2xvcjogcmdiYSgwLCAwLCAwLCAwLjU0KTtcbiAgZm9udC1zaXplOiAxNHB4OyB9XG5cbi5saXN0LWdyb3VwIC5saXN0LWdyb3VwLWl0ZW0gLmxpc3QtZ3JvdXAtaXRlbS1oZWFkaW5nIHtcbiAgY29sb3I6IHJnYmEoMCwgMCwgMCwgMC43Nyk7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgbGluZS1oZWlnaHQ6IDI5cHg7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtc2VwYXJhdG9yIHtcbiAgY2xlYXI6IGJvdGg7XG4gIG92ZXJmbG93OiBoaWRkZW47XG4gIG1hcmdpbi10b3A6IDEwcHg7XG4gIG1hcmdpbi1ib3R0b206IDEwcHg7IH1cblxuLmxpc3QtZ3JvdXAgLmxpc3QtZ3JvdXAtc2VwYXJhdG9yOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIHdpZHRoOiBjYWxjKDEwMCUgLSA5MHB4KTtcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHJnYmEoMCwgMCwgMCwgMC4xKTtcbiAgZmxvYXQ6IHJpZ2h0OyB9XG5cbi5iZy1wcm9maWxlIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzM0OThEQiAhaW1wb3J0YW50O1xuICBoZWlnaHQ6IDE1MHB4O1xuICB6LWluZGV4OiAxOyB9XG5cbi5iZy1ib3R0b20ge1xuICBoZWlnaHQ6IDEwMHB4O1xuICBtYXJnaW4tbGVmdDogMzBweDsgfVxuXG4uaW1nLXByb2ZpbGUge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2sgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgYm9yZGVyLXJhZGl1czogNnB4O1xuICBtYXJnaW4tdG9wOiAtNTAlO1xuICBwYWRkaW5nOiAxcHg7XG4gIHZlcnRpY2FsLWFsaWduOiBib3R0b207XG4gIGJvcmRlcjogMnB4IHNvbGlkICNmZmY7XG4gIC1tb3otYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgY29sb3I6ICNmZmY7XG4gIHotaW5kZXg6IDI7IH1cblxuLnJvdy1mbG9hdCB7XG4gIG1hcmdpbi10b3A6IC00MHB4OyB9XG5cbi5leHBsb3JlIGEge1xuICBjb2xvcjogZ3JlZW47XG4gIGZvbnQtc2l6ZTogMTNweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDsgfVxuXG4udHdpdHRlciBhIHtcbiAgY29sb3I6ICM0MDk5RkY7IH1cblxuLmltZy1ib3gge1xuICBib3gtc2hhZG93OiAwIDNweCA2cHggcmdiYSgwLCAwLCAwLCAwLjE2KSwgMCAzcHggNnB4IHJnYmEoMCwgMCwgMCwgMC4yMyk7XG4gIGJvcmRlci1yYWRpdXM6IDJweDtcbiAgYm9yZGVyOiAwOyB9XG5cbmlvbi10YWItYnV0dG9uLnRhYi1zZWxlY3RlZC50YWItaGFzLWxhYmVsLnRhYi1oYXMtaWNvbi50YWItbGF5b3V0LWljb24tdG9wLmlvbi1hY3RpdmF0YWJsZS5oeWRyYXRlZCB7XG4gIGNvbG9yOiAjOGM4YzhjOyB9XG4iXX0= */"

/***/ }),

/***/ "./src/app/list/list.page.ts":
/*!***********************************!*\
  !*** ./src/app/list/list.page.ts ***!
  \***********************************/
/*! exports provided: ListPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListPage", function() { return ListPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_device_motion_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/device-motion/ngx */ "./node_modules/@ionic-native/device-motion/ngx/index.js");










var ListPage = /** @class */ (function () {
    function ListPage(menu, navCtrl, platform, commonService, router, loadingController, storage, formBuilder, datePipe, geolocation, deviceMotion) {
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.commonService = commonService;
        this.router = router;
        this.loadingController = loadingController;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.datePipe = datePipe;
        this.geolocation = geolocation;
        this.deviceMotion = deviceMotion;
        this.openMenu = false;
        this.showLiveUpdatePopup = false;
        this.menu.enable(true);
    }
    ListPage.prototype.searchFilter = function () {
        this.togglePopupMenu();
    };
    ListPage.prototype.togglePopupMenu = function () {
        if (!this.showLiveUpdatePopup) {
            return this.openMenu = !this.openMenu;
        }
    };
    ListPage.prototype.navigateTo = function (page) {
        this.router.navigate(['/' + page]);
    };
    ListPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-list',
            template: __webpack_require__(/*! ./list.page.html */ "./src/app/list/list.page.html"),
            styles: [__webpack_require__(/*! ./list.page.scss */ "./src/app/list/list.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"], _ionic_native_device_motion_ngx__WEBPACK_IMPORTED_MODULE_9__["DeviceMotion"]])
    ], ListPage);
    return ListPage;
}());



/***/ })

}]);
//# sourceMappingURL=list-list-module.js.map