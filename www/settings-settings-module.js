(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["settings-settings-module"],{

/***/ "./src/app/settings/settings.module.ts":
/*!*********************************************!*\
  !*** ./src/app/settings/settings.module.ts ***!
  \*********************************************/
/*! exports provided: SettingsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPageModule", function() { return SettingsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _settings_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./settings.page */ "./src/app/settings/settings.page.ts");







var routes = [
    {
        path: '',
        component: _settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]
    }
];
var SettingsPageModule = /** @class */ (function () {
    function SettingsPageModule() {
    }
    SettingsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_settings_page__WEBPACK_IMPORTED_MODULE_6__["SettingsPage"]]
        })
    ], SettingsPageModule);
    return SettingsPageModule;
}());



/***/ }),

/***/ "./src/app/settings/settings.page.html":
/*!*********************************************!*\
  !*** ./src/app/settings/settings.page.html ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header class=\"headers\">\n\n    <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"start\">\n        <ion-menu-button></ion-menu-button>\n      </ion-buttons>\n      <ion-title slot=\"start\">\n        Profile Details\n  \n      </ion-title>\n  \n   \n    </ion-toolbar>\n  </ion-header>\n  \n  <ion-content>\n    \n  </ion-content>"

/***/ }),

/***/ "./src/app/settings/settings.page.scss":
/*!*********************************************!*\
  !*** ./src/app/settings/settings.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bs-calltoaction {\n  position: relative;\n  width: auto;\n  padding: 15px 25px;\n  border: 1px solid black;\n  margin-top: 10px;\n  margin-bottom: 10px;\n  border-radius: 5px; }\n\n.bs-calltoaction > .row {\n  display: table;\n  width: calc(100% + 30px); }\n\n.bs-calltoaction > .row > [class^=\"col-\"],\n.bs-calltoaction > .row > [class*=\" col-\"] {\n  float: none;\n  display: table-cell;\n  vertical-align: middle; }\n\n.cta-contents {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n.cta-title {\n  margin: 0 auto 15px;\n  padding: 0; }\n\n.cta-desc {\n  padding: 0; }\n\n.cta-desc p:last-child {\n  margin-bottom: 0; }\n\n.cta-button {\n  padding-top: 10px;\n  padding-bottom: 10px; }\n\n@media (max-width: 991px) {\n  .bs-calltoaction > .row {\n    display: block;\n    width: auto; }\n  .bs-calltoaction > .row > [class^=\"col-\"],\n  .bs-calltoaction > .row > [class*=\" col-\"] {\n    float: none;\n    display: block;\n    vertical-align: middle;\n    position: relative; }\n  .cta-contents {\n    text-align: center; } }\n\n.bs-calltoaction.bs-calltoaction-default {\n  color: #333;\n  background-color: #fff;\n  border-color: #ccc; }\n\n.bs-calltoaction.bs-calltoaction-primary {\n  color: #fff;\n  background-color: #337ab7;\n  border-color: #2e6da4; }\n\n.bs-calltoaction.bs-calltoaction-info {\n  color: #fff;\n  background-color: #5bc0de;\n  border-color: #46b8da; }\n\n.bs-calltoaction.bs-calltoaction-success {\n  color: #fff;\n  background-color: #5cb85c;\n  border-color: #4cae4c; }\n\n.bs-calltoaction.bs-calltoaction-warning {\n  color: #fff;\n  background-color: #f0ad4e;\n  border-color: #eea236; }\n\n.bs-calltoaction.bs-calltoaction-danger {\n  color: #fff;\n  background-color: #d9534f;\n  border-color: #d43f3a; }\n\n.bs-calltoaction.bs-calltoaction-primary .cta-button .btn,\n.bs-calltoaction.bs-calltoaction-info .cta-button .btn,\n.bs-calltoaction.bs-calltoaction-success .cta-button .btn,\n.bs-calltoaction.bs-calltoaction-warning .cta-button .btn,\n.bs-calltoaction.bs-calltoaction-danger .cta-button .btn {\n  border-color: #fff; }\n\n.fb-profile img.fb-image-lg {\n  z-index: 0;\n  width: 100%;\n  margin-bottom: 10px; }\n\n.fb-image-profile {\n  margin: -90px 10px 0px 50px;\n  z-index: 9;\n  width: 20%; }\n\n@media (max-width: 768px) {\n  .fb-profile-text > h1 {\n    font-weight: 700;\n    font-size: 16px; }\n  .fb-image-profile {\n    margin: -45px 10px 0px 25px;\n    z-index: 9;\n    width: 20%; } }\n\n.card {\n  background-color: rgba(214, 224, 226, 0.2);\n  border-top-width: 0;\n  border-bottom-width: 2px;\n  border-radius: 3px;\n  box-shadow: none;\n  box-sizing: border-box; }\n\n.card .card-heading {\n  padding: 0 20px;\n  margin: 0; }\n\n.card .card-heading.simple {\n  font-size: 20px;\n  font-weight: 300;\n  color: #777;\n  border-bottom: 1px solid #e5e5e5; }\n\n.card .card-heading.image img {\n  display: inline-block;\n  width: 46px;\n  height: 46px;\n  margin-right: 15px;\n  vertical-align: top;\n  border: 0;\n  border-radius: 50%; }\n\n.card .card-heading.image .card-heading-header {\n  display: inline-block;\n  vertical-align: top; }\n\n.card .card-heading.image .card-heading-header h3 {\n  margin: 0;\n  font-size: 14px;\n  line-height: 16px;\n  color: #262626; }\n\n.card .card-heading.image .card-heading-header span {\n  font-size: 12px;\n  color: #999999; }\n\n.card .card-body {\n  padding: 0 20px;\n  margin-top: 20px; }\n\n.card .card-media {\n  padding: 0 20px;\n  margin: 0 -14px; }\n\n.card .card-media img {\n  max-width: 100%;\n  max-height: 100%; }\n\n.card .card-actions {\n  min-height: 30px;\n  padding: 0 20px 20px 20px;\n  margin: 20px 0 0 0; }\n\n.card .card-comments {\n  padding: 20px;\n  margin: 0;\n  background-color: #f8f8f8; }\n\n.card .card-comments .comments-collapse-toggle {\n  padding: 0;\n  margin: 0 20px 12px 20px; }\n\n.card .card-comments .comments-collapse-toggle a,\n.card .card-comments .comments-collapse-toggle span {\n  padding-right: 5px;\n  overflow: hidden;\n  font-size: 12px;\n  color: #999;\n  text-overflow: ellipsis;\n  white-space: nowrap; }\n\n.card-comments .media-heading {\n  font-size: 13px;\n  font-weight: bold; }\n\n.card.people {\n  position: relative;\n  display: inline-block;\n  width: 170px;\n  height: 300px;\n  padding-top: 0;\n  margin-left: 20px;\n  overflow: hidden;\n  vertical-align: top; }\n\n.card.people:first-child {\n  margin-left: 0; }\n\n.card.people .card-top {\n  position: absolute;\n  top: 0;\n  left: 0;\n  display: inline-block;\n  width: 170px;\n  height: 150px;\n  background-color: #ffffff; }\n\n.card.people .card-top.green {\n  background-color: #53a93f; }\n\n.card.people .card-top.blue {\n  background-color: #427fed; }\n\n.card.people .card-info {\n  position: absolute;\n  top: 150px;\n  display: inline-block;\n  width: 100%;\n  height: 101px;\n  overflow: hidden;\n  background: #ffffff;\n  box-sizing: border-box; }\n\n.card.people .card-info .title {\n  display: block;\n  margin: 8px 14px 0 14px;\n  overflow: hidden;\n  font-size: 16px;\n  font-weight: bold;\n  line-height: 18px;\n  color: #404040; }\n\n.card.people .card-info .desc {\n  display: block;\n  margin: 8px 14px 0 14px;\n  overflow: hidden;\n  font-size: 12px;\n  line-height: 16px;\n  color: #737373;\n  text-overflow: ellipsis; }\n\n.card.people .card-bottom {\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  display: inline-block;\n  width: 100%;\n  padding: 10px 20px;\n  line-height: 29px;\n  text-align: center;\n  box-sizing: border-box; }\n\n.card.hovercard {\n  position: relative;\n  padding-top: 0;\n  overflow: hidden;\n  text-align: center;\n  background-color: rgba(214, 224, 226, 0.2); }\n\n.card.hovercard .cardheader {\n  background: url(\"https://api.boodskap.io/files/public/download/471233c1-7933-493f-9852-4c5ac6aa1352\");\n  background-size: cover;\n  height: 200px; }\n\n.card.hovercard .avatar {\n  position: relative;\n  top: -50px;\n  margin-bottom: -50px; }\n\n.card.hovercard .avatar img {\n  width: 100px;\n  height: 100px;\n  max-width: 100px;\n  max-height: 100px;\n  border-radius: 50%;\n  border: 5px solid rgba(255, 255, 255, 0.5); }\n\n.card.hovercard .info .title {\n  margin-bottom: 4px;\n  font-size: 24px;\n  line-height: 1;\n  color: #262626;\n  vertical-align: middle; }\n\n.card.hovercard .info .desc {\n  overflow: hidden;\n  font-size: 12px;\n  line-height: 20px;\n  color: #737373;\n  text-overflow: ellipsis; }\n\n.card.hovercard .bottom {\n  padding: 0 20px;\n  margin-bottom: 17px; }\n\n.btn {\n  border-radius: 50%;\n  width: 32px;\n  height: 32px;\n  line-height: 18px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9ob21lL2JhdmFuaS9EZXNrdG9wL2Jhc2UtZnJhbWV3b3JrLWlvbmljL3NyYy9hcHAvc2V0dGluZ3Mvc2V0dGluZ3MucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQWtCO0VBQ2xCLFdBQVU7RUFDVixrQkFBa0I7RUFDbEIsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixtQkFBbUI7RUFDbkIsa0JBQWtCLEVBQUE7O0FBR2xCO0VBQ0ksY0FBYTtFQUNiLHdCQUF3QixFQUFBOztBQUd4Qjs7RUFFSSxXQUFVO0VBQ1YsbUJBQWtCO0VBQ2xCLHNCQUFxQixFQUFBOztBQUdyQjtFQUNJLGlCQUFpQjtFQUNqQixvQkFBb0IsRUFBQTs7QUFHcEI7RUFDSSxtQkFBbUI7RUFDbkIsVUFBVSxFQUFBOztBQUdkO0VBQ0ksVUFBVSxFQUFBOztBQUdkO0VBQ0ksZ0JBQWdCLEVBQUE7O0FBR3hCO0VBQ0ksaUJBQWlCO0VBQ2pCLG9CQUFvQixFQUFBOztBQUdwQztFQUNJO0lBQ0ksY0FBYTtJQUNiLFdBQVcsRUFBQTtFQUdYOztJQUVJLFdBQVU7SUFDVixjQUFhO0lBQ2Isc0JBQXFCO0lBQ3JCLGtCQUFrQixFQUFBO0VBR3RCO0lBQ0ksa0JBQWtCLEVBQUEsRUFDckI7O0FBS1Q7RUFDSSxXQUFXO0VBQ1gsc0JBQXNCO0VBQ3RCLGtCQUFrQixFQUFBOztBQUd0QjtFQUNJLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIscUJBQXFCLEVBQUE7O0FBR3pCO0VBQ0ksV0FBVztFQUNYLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFHekI7RUFDSSxXQUFXO0VBQ1gseUJBQXlCO0VBQ3pCLHFCQUFxQixFQUFBOztBQUd6QjtFQUNJLFdBQVc7RUFDWCx5QkFBeUI7RUFDekIscUJBQXFCLEVBQUE7O0FBR3pCO0VBQ0ksV0FBVztFQUNYLHlCQUF5QjtFQUN6QixxQkFBcUIsRUFBQTs7QUFHekI7Ozs7O0VBS0ksa0JBQWlCLEVBQUE7O0FBR3JCO0VBQ0ksVUFBVTtFQUNWLFdBQVc7RUFDWCxtQkFBbUIsRUFBQTs7QUFHdkI7RUFFSSwyQkFBMkI7RUFDM0IsVUFBVTtFQUNWLFVBQVUsRUFBQTs7QUFHZDtFQUdBO0lBQ0ksZ0JBQWdCO0lBQ2hCLGVBQWMsRUFBQTtFQUdsQjtJQUVJLDJCQUEyQjtJQUMzQixVQUFVO0lBQ1YsVUFBVSxFQUFBLEVBQ2I7O0FBR0Q7RUFDSSwwQ0FBMEM7RUFDMUMsbUJBQW1CO0VBQ25CLHdCQUF3QjtFQUd4QixrQkFBa0I7RUFHbEIsZ0JBQWdCO0VBR2hCLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLGVBQWU7RUFDZixTQUFTLEVBQUE7O0FBR2I7RUFDSSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLFdBQVc7RUFDWCxnQ0FBZ0MsRUFBQTs7QUFHcEM7RUFDSSxxQkFBcUI7RUFDckIsV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLFNBQVM7RUFHVCxrQkFBa0IsRUFBQTs7QUFHdEI7RUFDSSxxQkFBcUI7RUFDckIsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksU0FBUztFQUNULGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYyxFQUFBOztBQUdsQjtFQUNJLGVBQWU7RUFDZixjQUFjLEVBQUE7O0FBR2xCO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGVBQWU7RUFDZixlQUFlLEVBQUE7O0FBR25CO0VBQ0ksZUFBZTtFQUNmLGdCQUFnQixFQUFBOztBQUdwQjtFQUNJLGdCQUFnQjtFQUNoQix5QkFBeUI7RUFDekIsa0JBQWtCLEVBQUE7O0FBR3RCO0VBQ0ksYUFBYTtFQUNiLFNBQVM7RUFDVCx5QkFBeUIsRUFBQTs7QUFHN0I7RUFDSSxVQUFVO0VBQ1Ysd0JBQXdCLEVBQUE7O0FBRzVCOztFQUVJLGtCQUFrQjtFQUNsQixnQkFBZ0I7RUFDaEIsZUFBZTtFQUNmLFdBQVc7RUFDWCx1QkFBdUI7RUFDdkIsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQ0ksZUFBZTtFQUNmLGlCQUFpQixFQUFBOztBQUdyQjtFQUNJLGtCQUFrQjtFQUNsQixxQkFBcUI7RUFDckIsWUFBWTtFQUNaLGFBQWE7RUFDYixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixtQkFBbUIsRUFBQTs7QUFHdkI7RUFDSSxjQUFjLEVBQUE7O0FBR2xCO0VBQ0ksa0JBQWtCO0VBQ2xCLE1BQU07RUFDTixPQUFPO0VBQ1AscUJBQXFCO0VBQ3JCLFlBQVk7RUFDWixhQUFhO0VBQ2IseUJBQXlCLEVBQUE7O0FBRzdCO0VBQ0kseUJBQXlCLEVBQUE7O0FBRzdCO0VBQ0kseUJBQXlCLEVBQUE7O0FBRzdCO0VBQ0ksa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixxQkFBcUI7RUFDckIsV0FBVztFQUNYLGFBQWE7RUFDYixnQkFBZ0I7RUFDaEIsbUJBQW1CO0VBR25CLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLGNBQWM7RUFDZCx1QkFBdUI7RUFDdkIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsaUJBQWlCO0VBQ2pCLGNBQWMsRUFBQTs7QUFHbEI7RUFDSSxjQUFjO0VBQ2QsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCx1QkFBdUIsRUFBQTs7QUFHM0I7RUFDSSxrQkFBa0I7RUFDbEIsU0FBUztFQUNULE9BQU87RUFDUCxxQkFBcUI7RUFDckIsV0FBVztFQUNYLGtCQUFrQjtFQUNsQixpQkFBaUI7RUFDakIsa0JBQWtCO0VBR2xCLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLGtCQUFrQjtFQUNsQixjQUFjO0VBQ2QsZ0JBQWdCO0VBQ2hCLGtCQUFrQjtFQUNsQiwwQ0FBMEMsRUFBQTs7QUFHOUM7RUFDSSxxR0FBcUc7RUFDckcsc0JBQXNCO0VBQ3RCLGFBQWEsRUFBQTs7QUFHakI7RUFDSSxrQkFBa0I7RUFDbEIsVUFBVTtFQUNWLG9CQUFvQixFQUFBOztBQUd4QjtFQUNJLFlBQVk7RUFDWixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUdqQixrQkFBa0I7RUFDbEIsMENBQXVDLEVBQUE7O0FBSzNDO0VBQ0ksa0JBQWtCO0VBQ2xCLGVBQWU7RUFDZixjQUFjO0VBQ2QsY0FBYztFQUNkLHNCQUFzQixFQUFBOztBQUcxQjtFQUNJLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLGNBQWM7RUFDZCx1QkFBdUIsRUFBQTs7QUFHM0I7RUFDSSxlQUFlO0VBQ2YsbUJBQW1CLEVBQUE7O0FBR3ZCO0VBQU0sa0JBQWtCO0VBQUUsV0FBVTtFQUFFLFlBQVc7RUFBRSxpQkFBZ0IsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3NldHRpbmdzL3NldHRpbmdzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5icy1jYWxsdG9hY3Rpb257XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHdpZHRoOmF1dG87XG4gICAgcGFkZGluZzogMTVweCAyNXB4O1xuICAgIGJvcmRlcjogMXB4IHNvbGlkIGJsYWNrO1xuICAgIG1hcmdpbi10b3A6IDEwcHg7XG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbiAgICBib3JkZXItcmFkaXVzOiA1cHg7XG59XG5cbiAgICAuYnMtY2FsbHRvYWN0aW9uID4gLnJvd3tcbiAgICAgICAgZGlzcGxheTp0YWJsZTtcbiAgICAgICAgd2lkdGg6IGNhbGMoMTAwJSArIDMwcHgpO1xuICAgIH1cbiAgICAgXG4gICAgICAgIC5icy1jYWxsdG9hY3Rpb24gPiAucm93ID4gW2NsYXNzXj1cImNvbC1cIl0sXG4gICAgICAgIC5icy1jYWxsdG9hY3Rpb24gPiAucm93ID4gW2NsYXNzKj1cIiBjb2wtXCJde1xuICAgICAgICAgICAgZmxvYXQ6bm9uZTtcbiAgICAgICAgICAgIGRpc3BsYXk6dGFibGUtY2VsbDtcbiAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOm1pZGRsZTtcbiAgICAgICAgfVxuXG4gICAgICAgICAgICAuY3RhLWNvbnRlbnRze1xuICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLmN0YS10aXRsZXtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG8gMTVweDtcbiAgICAgICAgICAgICAgICAgICAgcGFkZGluZzogMDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAuY3RhLWRlc2N7XG4gICAgICAgICAgICAgICAgICAgIHBhZGRpbmc6IDA7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgLmN0YS1kZXNjIHA6bGFzdC1jaGlsZHtcbiAgICAgICAgICAgICAgICAgICAgbWFyZ2luLWJvdHRvbTogMDtcbiAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIC5jdGEtYnV0dG9ue1xuICAgICAgICAgICAgICAgIHBhZGRpbmctdG9wOiAxMHB4O1xuICAgICAgICAgICAgICAgIHBhZGRpbmctYm90dG9tOiAxMHB4O1xuICAgICAgICAgICAgfVxuXG5AbWVkaWEgKG1heC13aWR0aDogOTkxcHgpe1xuICAgIC5icy1jYWxsdG9hY3Rpb24gPiAucm93e1xuICAgICAgICBkaXNwbGF5OmJsb2NrO1xuICAgICAgICB3aWR0aDogYXV0bztcbiAgICB9XG5cbiAgICAgICAgLmJzLWNhbGx0b2FjdGlvbiA+IC5yb3cgPiBbY2xhc3NePVwiY29sLVwiXSxcbiAgICAgICAgLmJzLWNhbGx0b2FjdGlvbiA+IC5yb3cgPiBbY2xhc3MqPVwiIGNvbC1cIl17XG4gICAgICAgICAgICBmbG9hdDpub25lO1xuICAgICAgICAgICAgZGlzcGxheTpibG9jaztcbiAgICAgICAgICAgIHZlcnRpY2FsLWFsaWduOm1pZGRsZTtcbiAgICAgICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIC5jdGEtY29udGVudHN7XG4gICAgICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICAgIH1cbn1cblxuXG5cbi5icy1jYWxsdG9hY3Rpb24uYnMtY2FsbHRvYWN0aW9uLWRlZmF1bHR7XG4gICAgY29sb3I6ICMzMzM7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbiAgICBib3JkZXItY29sb3I6ICNjY2M7XG59XG5cbi5icy1jYWxsdG9hY3Rpb24uYnMtY2FsbHRvYWN0aW9uLXByaW1hcnl7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzMzN2FiNztcbiAgICBib3JkZXItY29sb3I6ICMyZTZkYTQ7XG59XG5cbi5icy1jYWxsdG9hY3Rpb24uYnMtY2FsbHRvYWN0aW9uLWluZm97XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzViYzBkZTtcbiAgICBib3JkZXItY29sb3I6ICM0NmI4ZGE7XG59XG5cbi5icy1jYWxsdG9hY3Rpb24uYnMtY2FsbHRvYWN0aW9uLXN1Y2Nlc3N7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzVjYjg1YztcbiAgICBib3JkZXItY29sb3I6ICM0Y2FlNGM7XG59XG5cbi5icy1jYWxsdG9hY3Rpb24uYnMtY2FsbHRvYWN0aW9uLXdhcm5pbmd7XG4gICAgY29sb3I6ICNmZmY7XG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YwYWQ0ZTtcbiAgICBib3JkZXItY29sb3I6ICNlZWEyMzY7XG59XG5cbi5icy1jYWxsdG9hY3Rpb24uYnMtY2FsbHRvYWN0aW9uLWRhbmdlcntcbiAgICBjb2xvcjogI2ZmZjtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZDk1MzRmO1xuICAgIGJvcmRlci1jb2xvcjogI2Q0M2YzYTtcbn1cblxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24tcHJpbWFyeSAuY3RhLWJ1dHRvbiAuYnRuLFxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24taW5mbyAuY3RhLWJ1dHRvbiAuYnRuLFxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24tc3VjY2VzcyAuY3RhLWJ1dHRvbiAuYnRuLFxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24td2FybmluZyAuY3RhLWJ1dHRvbiAuYnRuLFxuLmJzLWNhbGx0b2FjdGlvbi5icy1jYWxsdG9hY3Rpb24tZGFuZ2VyIC5jdGEtYnV0dG9uIC5idG57XG4gICAgYm9yZGVyLWNvbG9yOiNmZmY7XG59XG5cbi5mYi1wcm9maWxlIGltZy5mYi1pbWFnZS1sZ3tcbiAgICB6LWluZGV4OiAwO1xuICAgIHdpZHRoOiAxMDAlOyAgXG4gICAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn1cblxuLmZiLWltYWdlLXByb2ZpbGVcbntcbiAgICBtYXJnaW46IC05MHB4IDEwcHggMHB4IDUwcHg7XG4gICAgei1pbmRleDogOTtcbiAgICB3aWR0aDogMjAlOyBcbn1cblxuQG1lZGlhIChtYXgtd2lkdGg6NzY4cHgpXG57XG4gICAgXG4uZmItcHJvZmlsZS10ZXh0Pmgxe1xuICAgIGZvbnQtd2VpZ2h0OiA3MDA7XG4gICAgZm9udC1zaXplOjE2cHg7XG59XG5cbi5mYi1pbWFnZS1wcm9maWxlXG57XG4gICAgbWFyZ2luOiAtNDVweCAxMHB4IDBweCAyNXB4O1xuICAgIHotaW5kZXg6IDk7XG4gICAgd2lkdGg6IDIwJTsgXG59XG59XG5cbi5jYXJkIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2JhKDIxNCwgMjI0LCAyMjYsIDAuMik7XG4gICAgYm9yZGVyLXRvcC13aWR0aDogMDtcbiAgICBib3JkZXItYm90dG9tLXdpZHRoOiAycHg7XG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiAzcHg7XG4gICAgYm9yZGVyLXJhZGl1czogM3B4O1xuICAgIC13ZWJraXQtYm94LXNoYWRvdzogbm9uZTtcbiAgICAtbW96LWJveC1zaGFkb3c6IG5vbmU7XG4gICAgYm94LXNoYWRvdzogbm9uZTtcbiAgICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG5cbi5jYXJkIC5jYXJkLWhlYWRpbmcge1xuICAgIHBhZGRpbmc6IDAgMjBweDtcbiAgICBtYXJnaW46IDA7XG59XG5cbi5jYXJkIC5jYXJkLWhlYWRpbmcuc2ltcGxlIHtcbiAgICBmb250LXNpemU6IDIwcHg7XG4gICAgZm9udC13ZWlnaHQ6IDMwMDtcbiAgICBjb2xvcjogIzc3NztcbiAgICBib3JkZXItYm90dG9tOiAxcHggc29saWQgI2U1ZTVlNTtcbn1cblxuLmNhcmQgLmNhcmQtaGVhZGluZy5pbWFnZSBpbWcge1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogNDZweDtcbiAgICBoZWlnaHQ6IDQ2cHg7XG4gICAgbWFyZ2luLXJpZ2h0OiAxNXB4O1xuICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XG4gICAgYm9yZGVyOiAwO1xuICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIC1tb3otYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuLmNhcmQgLmNhcmQtaGVhZGluZy5pbWFnZSAuY2FyZC1oZWFkaW5nLWhlYWRlciB7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHZlcnRpY2FsLWFsaWduOiB0b3A7XG59XG5cbi5jYXJkIC5jYXJkLWhlYWRpbmcuaW1hZ2UgLmNhcmQtaGVhZGluZy1oZWFkZXIgaDMge1xuICAgIG1hcmdpbjogMDtcbiAgICBmb250LXNpemU6IDE0cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgY29sb3I6ICMyNjI2MjY7XG59XG5cbi5jYXJkIC5jYXJkLWhlYWRpbmcuaW1hZ2UgLmNhcmQtaGVhZGluZy1oZWFkZXIgc3BhbiB7XG4gICAgZm9udC1zaXplOiAxMnB4O1xuICAgIGNvbG9yOiAjOTk5OTk5O1xufVxuXG4uY2FyZCAuY2FyZC1ib2R5IHtcbiAgICBwYWRkaW5nOiAwIDIwcHg7XG4gICAgbWFyZ2luLXRvcDogMjBweDtcbn1cblxuLmNhcmQgLmNhcmQtbWVkaWEge1xuICAgIHBhZGRpbmc6IDAgMjBweDtcbiAgICBtYXJnaW46IDAgLTE0cHg7XG59XG5cbi5jYXJkIC5jYXJkLW1lZGlhIGltZyB7XG4gICAgbWF4LXdpZHRoOiAxMDAlO1xuICAgIG1heC1oZWlnaHQ6IDEwMCU7XG59XG5cbi5jYXJkIC5jYXJkLWFjdGlvbnMge1xuICAgIG1pbi1oZWlnaHQ6IDMwcHg7XG4gICAgcGFkZGluZzogMCAyMHB4IDIwcHggMjBweDtcbiAgICBtYXJnaW46IDIwcHggMCAwIDA7XG59XG5cbi5jYXJkIC5jYXJkLWNvbW1lbnRzIHtcbiAgICBwYWRkaW5nOiAyMHB4O1xuICAgIG1hcmdpbjogMDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjhmOGY4O1xufVxuXG4uY2FyZCAuY2FyZC1jb21tZW50cyAuY29tbWVudHMtY29sbGFwc2UtdG9nZ2xlIHtcbiAgICBwYWRkaW5nOiAwO1xuICAgIG1hcmdpbjogMCAyMHB4IDEycHggMjBweDtcbn1cblxuLmNhcmQgLmNhcmQtY29tbWVudHMgLmNvbW1lbnRzLWNvbGxhcHNlLXRvZ2dsZSBhLFxuLmNhcmQgLmNhcmQtY29tbWVudHMgLmNvbW1lbnRzLWNvbGxhcHNlLXRvZ2dsZSBzcGFuIHtcbiAgICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgY29sb3I6ICM5OTk7XG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbn1cblxuLmNhcmQtY29tbWVudHMgLm1lZGlhLWhlYWRpbmcge1xuICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmNhcmQucGVvcGxlIHtcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gICAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xuICAgIHdpZHRoOiAxNzBweDtcbiAgICBoZWlnaHQ6IDMwMHB4O1xuICAgIHBhZGRpbmctdG9wOiAwO1xuICAgIG1hcmdpbi1sZWZ0OiAyMHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgdmVydGljYWwtYWxpZ246IHRvcDtcbn1cblxuLmNhcmQucGVvcGxlOmZpcnN0LWNoaWxkIHtcbiAgICBtYXJnaW4tbGVmdDogMDtcbn1cblxuLmNhcmQucGVvcGxlIC5jYXJkLXRvcCB7XG4gICAgcG9zaXRpb246IGFic29sdXRlO1xuICAgIHRvcDogMDtcbiAgICBsZWZ0OiAwO1xuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgICB3aWR0aDogMTcwcHg7XG4gICAgaGVpZ2h0OiAxNTBweDtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmZmZmO1xufVxuXG4uY2FyZC5wZW9wbGUgLmNhcmQtdG9wLmdyZWVuIHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjNTNhOTNmO1xufVxuXG4uY2FyZC5wZW9wbGUgLmNhcmQtdG9wLmJsdWUge1xuICAgIGJhY2tncm91bmQtY29sb3I6ICM0MjdmZWQ7XG59XG5cbi5jYXJkLnBlb3BsZSAuY2FyZC1pbmZvIHtcbiAgICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gICAgdG9wOiAxNTBweDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgaGVpZ2h0OiAxMDFweDtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGJhY2tncm91bmQ6ICNmZmZmZmY7XG4gICAgLXdlYmtpdC1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIC1tb3otYm94LXNpemluZzogYm9yZGVyLWJveDtcbiAgICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xufVxuXG4uY2FyZC5wZW9wbGUgLmNhcmQtaW5mbyAudGl0bGUge1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIG1hcmdpbjogOHB4IDE0cHggMCAxNHB4O1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgZm9udC1zaXplOiAxNnB4O1xuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xuICAgIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICAgIGNvbG9yOiAjNDA0MDQwO1xufVxuXG4uY2FyZC5wZW9wbGUgLmNhcmQtaW5mbyAuZGVzYyB7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgbWFyZ2luOiA4cHggMTRweCAwIDE0cHg7XG4gICAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgICBmb250LXNpemU6IDEycHg7XG4gICAgbGluZS1oZWlnaHQ6IDE2cHg7XG4gICAgY29sb3I6ICM3MzczNzM7XG4gICAgdGV4dC1vdmVyZmxvdzogZWxsaXBzaXM7XG59XG5cbi5jYXJkLnBlb3BsZSAuY2FyZC1ib3R0b20ge1xuICAgIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgICBib3R0b206IDA7XG4gICAgbGVmdDogMDtcbiAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gICAgcGFkZGluZzogMTBweCAyMHB4O1xuICAgIGxpbmUtaGVpZ2h0OiAyOXB4O1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgICAtd2Via2l0LWJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gICAgLW1vei1ib3gtc2l6aW5nOiBib3JkZXItYm94O1xuICAgIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG5cbi5jYXJkLmhvdmVyY2FyZCB7XG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xuICAgIHBhZGRpbmctdG9wOiAwO1xuICAgIG92ZXJmbG93OiBoaWRkZW47XG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMjE0LCAyMjQsIDIyNiwgMC4yKTtcbn1cblxuLmNhcmQuaG92ZXJjYXJkIC5jYXJkaGVhZGVyIHtcbiAgICBiYWNrZ3JvdW5kOiB1cmwoXCJodHRwczovL2FwaS5ib29kc2thcC5pby9maWxlcy9wdWJsaWMvZG93bmxvYWQvNDcxMjMzYzEtNzkzMy00OTNmLTk4NTItNGM1YWM2YWExMzUyXCIpO1xuICAgIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICAgaGVpZ2h0OiAyMDBweDtcbn1cblxuLmNhcmQuaG92ZXJjYXJkIC5hdmF0YXIge1xuICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgICB0b3A6IC01MHB4O1xuICAgIG1hcmdpbi1ib3R0b206IC01MHB4O1xufVxuXG4uY2FyZC5ob3ZlcmNhcmQgLmF2YXRhciBpbWcge1xuICAgIHdpZHRoOiAxMDBweDtcbiAgICBoZWlnaHQ6IDEwMHB4O1xuICAgIG1heC13aWR0aDogMTAwcHg7XG4gICAgbWF4LWhlaWdodDogMTAwcHg7XG4gICAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgLW1vei1ib3JkZXItcmFkaXVzOiA1MCU7XG4gICAgYm9yZGVyLXJhZGl1czogNTAlO1xuICAgIGJvcmRlcjogNXB4IHNvbGlkIHJnYmEoMjU1LDI1NSwyNTUsMC41KTtcbn1cblxuXG5cbi5jYXJkLmhvdmVyY2FyZCAuaW5mbyAudGl0bGUge1xuICAgIG1hcmdpbi1ib3R0b206IDRweDtcbiAgICBmb250LXNpemU6IDI0cHg7XG4gICAgbGluZS1oZWlnaHQ6IDE7XG4gICAgY29sb3I6ICMyNjI2MjY7XG4gICAgdmVydGljYWwtYWxpZ246IG1pZGRsZTtcbn1cblxuLmNhcmQuaG92ZXJjYXJkIC5pbmZvIC5kZXNjIHtcbiAgICBvdmVyZmxvdzogaGlkZGVuO1xuICAgIGZvbnQtc2l6ZTogMTJweDtcbiAgICBsaW5lLWhlaWdodDogMjBweDtcbiAgICBjb2xvcjogIzczNzM3MztcbiAgICB0ZXh0LW92ZXJmbG93OiBlbGxpcHNpcztcbn1cblxuLmNhcmQuaG92ZXJjYXJkIC5ib3R0b20ge1xuICAgIHBhZGRpbmc6IDAgMjBweDtcbiAgICBtYXJnaW4tYm90dG9tOiAxN3B4O1xufVxuXG4uYnRueyBib3JkZXItcmFkaXVzOiA1MCU7IHdpZHRoOjMycHg7IGhlaWdodDozMnB4OyBsaW5lLWhlaWdodDoxOHB4OyAgfVxuIl19 */"

/***/ }),

/***/ "./src/app/settings/settings.page.ts":
/*!*******************************************!*\
  !*** ./src/app/settings/settings.page.ts ***!
  \*******************************************/
/*! exports provided: SettingsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsPage", function() { return SettingsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_common_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../services/common.service */ "./src/app/services/common.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_native_device_motion_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/device-motion/ngx */ "./node_modules/@ionic-native/device-motion/ngx/index.js");










var SettingsPage = /** @class */ (function () {
    function SettingsPage(menu, navCtrl, platform, commonService, router, loadingController, storage, formBuilder, datePipe, geolocation, deviceMotion) {
        this.menu = menu;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.commonService = commonService;
        this.router = router;
        this.loadingController = loadingController;
        this.storage = storage;
        this.formBuilder = formBuilder;
        this.datePipe = datePipe;
        this.geolocation = geolocation;
        this.deviceMotion = deviceMotion;
        this.openMenu = false;
        this.showLiveUpdatePopup = false;
        this.menu.enable(true);
    }
    SettingsPage.prototype.searchFilter = function () {
        this.togglePopupMenu();
    };
    SettingsPage.prototype.togglePopupMenu = function () {
        if (!this.showLiveUpdatePopup) {
            return this.openMenu = !this.openMenu;
        }
    };
    SettingsPage.prototype.navigateTo = function (page) {
        this.router.navigate(['/' + page]);
    };
    SettingsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-settings',
            template: __webpack_require__(/*! ./settings.page.html */ "./src/app/settings/settings.page.html"),
            styles: [__webpack_require__(/*! ./settings.page.scss */ "./src/app/settings/settings.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["MenuController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"], _services_common_service__WEBPACK_IMPORTED_MODULE_3__["CommonService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"], _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"], _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormBuilder"], _angular_common__WEBPACK_IMPORTED_MODULE_7__["DatePipe"], _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_8__["Geolocation"], _ionic_native_device_motion_ngx__WEBPACK_IMPORTED_MODULE_9__["DeviceMotion"]])
    ], SettingsPage);
    return SettingsPage;
}());



/***/ })

}]);
//# sourceMappingURL=settings-settings-module.js.map